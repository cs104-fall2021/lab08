english_to_french = {"bread": "pain", "wine": "vin", "with": "avec", "I": "Je",
                     "eat": "mange", "drink": "bois", "John": "Jean",
                     "friends": "amis", "and": "et", "of": "du", "red": "rouge"}


def is_alphabetical(character):
    return character in alphabet


def translate_to_french(input_in_english):
    output_in_french = ""
    for word in input_in_english.split():
        translated = ""
        if not is_alphabetical(word.lower()[-1]):
            translated = english_to_french[word[:-1]]
            translated += word[-1]
        else:
            translated = english_to_french[word]
        output_in_french += translated + " "

    return output_in_french


my_sentence_in_english = "John and, friends drink wine."
# Expect "Jean et, amis bois vin."
# print(translate_to_french(my_sentence_in_english))
my_sentence_in_english = "I eat bread!"
# Expect "Je mange pain!"
print(translate_to_french(my_sentence_in_english))

