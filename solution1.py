alphabet = "abcdefghijklmnopqrstuvwxyz"


def count_letters(my_word):
    my_dict = dict()
    for c in my_word.lower():
        if c in alphabet:
            if c not in my_dict:
                my_dict[c] = 1
            else:
                my_dict[c] += 1
    for key in sorted(list(my_dict.keys())):
        print(key, my_dict[key])
    print()


# input = "hello"
# output = h:1, e:1, l:2, o:1
input_string = "Hello!"
# count_letters(input_string)
# input = "The cat in the hat"
# output = a:2, c:1, e:2, h:3, i:1, n:1, t:4
input_string = "The cat in the hat"
# count_letters(input_string)
my_abstract = "In the literature, anisotropy-invariant maps are being proposed to represent a domain within which all realizable Reynolds stress invariants must lie. It is shown that the representation proposed by  Lumley and Newman has disadvantages owing to the fact that the anisotropy invariants (II, III) (II, III) are one-to-one uniquely interdependent, and as a result satisfies the requirement of realizability."
# count_letters(my_abstract)

